﻿public class WorkAction : Action
{
    public override float CalculateUtility(Character character)
    {
        float value = evaluator.Evaluate(character.Stats.Energy);
        character.Utilities.Energy = value;
        return value;
    }
}