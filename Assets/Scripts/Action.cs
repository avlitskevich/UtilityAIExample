﻿using System;
using UnityEngine;

public abstract class Action : MonoBehaviour
{
    [SerializeField] private float sleepEffect;
    [SerializeField] private float hungerEffect;
    [SerializeField] private float energyEffect;
    [SerializeField] private float bladderEffect;

    [SerializeField] private float cooldown;
    [SerializeField] private Color color;
    [SerializeField] protected AnimationCurve evaluator;


    private void Awake()
    {
        GetComponent<Renderer>().material.color = color;
    }


    public abstract float CalculateUtility(Character character);

    public void Act(Character character)
    {
        character.Color = color;

        if (!IsCharCloseEnough(character))
        {
            character.Agent.SetDestination(transform.position);
            return;
        }
        
        character.Stats.Sleep += sleepEffect;
        character.Stats.Hunger += hungerEffect;
        character.Stats.Energy += energyEffect;
        character.Stats.Bladder += bladderEffect;

        character.Cooldown = cooldown;
    }

    private bool IsCharCloseEnough(Character character)
    {
        return Vector3.Distance(character.transform.position, transform.position) < 2f;
    }
}