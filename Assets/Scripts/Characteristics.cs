﻿using System;
using UnityEngine;

[Serializable]
public class Characteristics
{
    public float Sleep
    {
        get => sleep;
        set => sleep = Mathf.Clamp01(value);
    }
    public float Hunger
    {
        get => hunger;
        set => hunger = Mathf.Clamp01(value);
    }
    public float Energy
    {
        get => energy;
        set => energy = Mathf.Clamp01(value);
    }
    public float Bladder
    {
        get => bladder;
        set => bladder = Mathf.Clamp01(value);
    }
    
    public float sleep;
    public float hunger;
    public float energy;
    public float bladder;
}