﻿using UnityEngine;

namespace Bot
{
    public class SimpleFlyingBehaviour : AbstractMovingBehaviour
    {
        [SerializeField] private float speed;
        [SerializeField] private float idleTime;
        [SerializeField] private float flyingRadius;

        private enum State
        {
            Moving, Idle, Stopped
        }
        
        private float idleTimer;
        private State currentState = State.Stopped;
        private Vector3 target;
        
        
        
        private void Update()
        {
            if (currentState == State.Stopped)
                return;

            if (idleTimer > 0)
            {
                idleTimer -= Time.deltaTime;
                return;
            }
            
            if (currentState == State.Idle)
            {
                target = RandomPointOnSphere();
                currentState = State.Moving;
            }

            if (Vector3.Distance(transform.position, target) < 2f)
            {
                idleTimer = Random.Range(0, idleTime);
                currentState = State.Idle;
                return;
            }

            Vector3 newPosition = transform.position + speed * Time.deltaTime * (target - transform.position);
            Vector3 offset = newPosition - TargetPlatform.position;

            transform.position = offset.normalized * flyingRadius + TargetPlatform.position;
            transform.LookAt(TargetPlatform.position);
        }


        
        private Vector3 RandomPointOnSphere()
        {
            return new Vector3(Random.Range(-1f, 1f),
                Random.Range(0f, .5f),
                Random.Range(-1f, 1f)).normalized * flyingRadius;
        }

        public override void SetActive(bool value)
        {
            currentState = value
                ? currentState == State.Stopped 
                    ? State.Idle
                    : currentState
                : State.Stopped;
        }
    }
}