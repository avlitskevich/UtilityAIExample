﻿using System;
using UnityEngine;

namespace Bot
{
    public class Bot : MonoBehaviour
    {
        [SerializeField] private bool isActive;
        [SerializeField] private AbstractMovingBehaviour movingBehaviour;
        [SerializeField] private Transform platform;

        
        private void Awake()
        {
            movingBehaviour.TargetPlatform = platform;
            movingBehaviour.SetActive(true);
        }

        private void OnValidate()
        {
            if (movingBehaviour == null)
                return;
            
            movingBehaviour.SetActive(isActive);
        }
    }
}