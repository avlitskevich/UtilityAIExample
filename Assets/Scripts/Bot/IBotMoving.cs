﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractMovingBehaviour : MonoBehaviour
{
    public Transform TargetPlatform { get; set; }
    public abstract void SetActive(bool value);
}