﻿using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Character : MonoBehaviour
{
    public Characteristics Stats => currentStats;
    public Characteristics Utilities => currentUtility;
    public NavMeshAgent Agent => agent;
    public float Cooldown
    {
        set => timer = value;
    }
    public Color Color
    {
        set => GetComponent<Renderer>().material.color = value;
    }

    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Characteristics currentStats;
    [SerializeField] private Characteristics currentUtility;
    [SerializeField] private Action[] actions;

    private float timer;
    

    private void Update()
    {
        AffectStats();

        if (timer > 0)
        {
            timer -= Time.deltaTime;
            return;
        }
        
        Action best = actions.OrderByDescending(x => x.CalculateUtility(this))
            .First();
        
        best.Act(this);
    }

    
    private void AffectStats()
    {
        currentStats.Sleep += 0.001f * Time.deltaTime;
        currentStats.Hunger += 0.001f * Time.deltaTime;
        currentStats.Energy += 0.001f * Time.deltaTime;
        currentStats.Bladder += 0.001f * Time.deltaTime;
    }
}