﻿public class SleepAction : Action
{
    public override float CalculateUtility(Character character)
    {
        float value = evaluator.Evaluate(character.Stats.Sleep);
        character.Utilities.Sleep = value;
        return value;
    }
}