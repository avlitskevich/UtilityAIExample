﻿public class EatAction : Action
{
    public override float CalculateUtility(Character character)
    {
        float value = evaluator.Evaluate(character.Stats.Hunger);
        character.Utilities.Hunger = value;
        return value;
    }
}