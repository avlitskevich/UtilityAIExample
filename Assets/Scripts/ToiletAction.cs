﻿public class ToiletAction : Action
{
    public override float CalculateUtility(Character character)
    {
        float value = evaluator.Evaluate(character.Stats.Bladder);
        character.Utilities.Bladder = value;
        return value;
    }
}